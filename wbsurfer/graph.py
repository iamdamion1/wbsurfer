from __future__ import annotations
from typing import List, Tuple, Union
from queue import Queue
from contextlib import nullcontext

import numpy as np
from trimesh import Trimesh
from networkx import DiGraph, Graph
from networkx.algorithms.shortest_paths import shortest_path, shortest_path_length
from networkx.algorithms.dag import dag_longest_path
from yaspin import yaspin
from yaspin.spinners import Spinners
from geodesic_chenhan import (  # type: ignore
    CRichModel as RichModel,
    CPoint3D as Point3D,
    CFace as Face,
    CICHWithFurtherPriorityQueue as ICHWithFurtherPriorityQueue,
)

from .images import Cifti, Gifti

# input radius, number of lines/angles
# output for every vertex on surface, give paths (ordered vertices) through vertex at radius for number of lines angles


def chenhan_shortest_path(
    vertices: Union[np.ndarray, List[List[float]]],
    faces: Union[np.ndarray, List[List[int]]],
    source: int,
    target: int,
    nearest_idx: bool = False,
) -> Union[List[int], List[float]]:
    """Use the chenhan algortihm for finding the shortest path."""
    # convert to numpy arrays
    vertices = np.array(vertices)
    faces = np.array(faces)

    # create model
    bmodel = RichModel()

    # get vertices and faces in proper format
    vertices_list = list()
    for v in vertices:
        vertices_list.append(Point3D(float(v[0]), float(v[1]), float(v[2])))
    faces_list = list()
    for f in faces:
        faces_list.append(Face(int(f[0]), int(f[1]), int(f[2])))

    # load mesh into model
    bmodel.LoadModel(vertices_list, faces_list)
    bmodel.Preprocess()

    # setup search
    emethod = ICHWithFurtherPriorityQueue(bmodel, set([int(source)]))
    emethod.Execute()

    # search for path from target to source
    path = emethod.FindSourceVertex(int(target), [])

    # reverse path to go from source to target
    path.reverse()

    # get the vertex locations
    vertex_locations = list()
    for p in path:
        pt = p.Get3DPoint(bmodel)
        vertex_locations.append((pt.x, pt.y, pt.z))

    # if nearest_idx, get nearest vertex interpolated chenhan path
    if nearest_idx:
        # interpolate to nearest vertex in chenhan path
        icp = list()
        for vertex in vertex_locations:
            icp.append(np.argmin(np.linalg.norm(vertices - np.array(vertex), axis=1)))

        # remove duplicates from icp
        last_vertex = -1
        nodups_icp = list()
        for i in icp:
            if i != last_vertex:
                nodups_icp.append(i)
                last_vertex = i

        # get graph from mesh
        graph = Trimesh(vertices=vertices, faces=faces, process=False).vertex_adjacency_graph
        iicp = list()
        for v1, v2 in zip(nodups_icp[:-1], nodups_icp[1:]):
            path = shortest_path(graph, source=v1, target=v2, weight="weight")
            iicp.extend(path[:-1])
        iicp.append(path[-1])

        # return indices of interpolated chenhan path
        return iicp
    else:
        # return chenhan vertex locations
        return vertex_locations


class ROIGraph:
    """Object representing a graph on an ROI."""

    @classmethod
    def create(cls, ciftiname: str, giftiname: str, *args, **kwargs) -> ROIGraph:
        cifti = Cifti(ciftiname)
        gifti = Gifti(giftiname)
        return cls(cifti, gifti, *args, **kwargs)

    def __init__(self, cifti: Cifti, gifti: Gifti, index: int = None, nospinner: bool = False) -> None:
        self._graph = None
        # store cifti/gifti
        self.cifti = cifti
        self.gifti = gifti

        # get copy of triangles on surface
        triangles = gifti.triangles.copy()

        # get the row data to check if vertex is relevant to the ROI
        if index:  # index is defined
            row_data = cifti.row_data.astype(int)[0]
            row_indices = np.argwhere(row_data == index)[:, 0]

            # get the surface that the selected row indices are on
            _, surface = cifti.row_to_vertex_index(row_indices[0])

            # check if surface index accessed is the same surface
            # as user loaded in, raise error if not
            if gifti.name != surface:
                raise ValueError(
                    "Index accessed does not match surface loaded!\n"
                    f"Surface Loaded: {gifti.name}\n"
                    f"Surface Accessed: {surface}\n"
                )

            # get the vertex/row table + row data
            if gifti.name == "CORTEX_LEFT":
                vrtable, start, end = cifti.get_vertex_row_table()[:3]
                # get row data
            elif gifti.name == "CORTEX_RIGHT":
                vrtable, start, end = cifti.get_vertex_row_table()[3:]
                row_indices -= start  # add offset to row_indices
            vertex_indices = vrtable[row_indices]
        else:  # use all indices
            # get the vertex/row table + row data
            if gifti.name == "CORTEX_LEFT":
                vrtable = cifti.get_vertex_row_table()[0]
                # get row data
            elif gifti.name == "CORTEX_RIGHT":
                vrtable = cifti.get_vertex_row_table()[3]
            vertex_indices = vrtable

        # create set out of vertex indices for faster look up
        vertex_set = set(vertex_indices)
        # convert triangles to edges
        edges = set()
        faces = set()
        face_normals = list()
        dfaces = set()
        load_text = "Building graph%s..." % (" of selected ROI" if index else "")
        if nospinner:
            cm = nullcontext()
        else:
            cm = yaspin(Spinners.dots12, text=load_text, color="blue")
        with cm as sp:
            for t, fn in zip(triangles, gifti.face_normals):
                # copy triangle ordering for faces
                nt = tuple(t.copy())
                t.sort()
                # only add edge if vertices is in vrtable
                # if the point is not in the vrtable it
                # usually means it is part of the medial wall
                # at least 1 vertex must also be in vertex_set
                # which is defined by the index
                if t[0] in vrtable and t[1] in vrtable and (t[0] in vertex_set or t[1] in vertex_set):
                    edges.add((t[0], t[1]))
                if t[1] in vrtable and t[2] in vrtable and (t[1] in vertex_set or t[2] in vertex_set):
                    edges.add((t[1], t[2]))
                if t[0] in vrtable and t[2] in vrtable and (t[0] in vertex_set or t[2] in vertex_set):
                    edges.add((t[0], t[2]))

                # TODO: This class is really messy now, probably split up this logic
                # extra logic for storing faces
                # check if all edges in edges list, if it is, then the triangle must exist
                if (t[0], t[1]) in edges and (t[1], t[2]) in edges and (t[0], t[2]) in edges:
                    faces.add(nt)
                    face_normals.append(fn)
                else:  # this triangle is deleted
                    dfaces.add(nt)
            if not nospinner:
                sp.text = "Graph built."
                sp.ok("✅")

        # return the nodes, and, edges of the relevant ROI
        self._vertex_indices = list(vertex_indices)
        self._edges = list(edges)

        # expose faces
        self.faces = np.array(list(faces))
        self.face_normals = np.array(face_normals)
        self.dfaces = np.array(list(dfaces))

        # make full graph on ROI
        self._graph = Graph(self._edges)

        # for each edge add the weights
        for edge in self._graph.edges:
            # compute length of edge
            length = np.linalg.norm(self.gifti.vertices[edge[0]] - self.gifti.vertices[edge[1]])
            self._graph[edge[0]][edge[1]]["weight"] = length

        # add vertex positions to graph
        for n in self._graph.nodes:
            self._graph.nodes[n]["position"] = self.gifti.vertices[n]

        # set centroid_index
        self._centroid_index = None

    @property
    def graph(self) -> Graph:
        return self._graph

    @property
    def vertex_indices(self) -> List[int]:
        return self._vertex_indices

    @property
    def vertices(self) -> np.ndarray:
        return self.gifti.vertices[self._vertex_indices]

    @property
    def edges(self) -> List[Tuple[int, int]]:
        return self._edges

    @property
    def centroid_index(self) -> int:
        if self._centroid_index is None:
            with yaspin(Spinners.dots12, text="Finding centroid of ROI...", color="blue") as sp:
                # get boundary points
                boundary_points = self.get_boundary()

                # initialize geodesic distances matrix
                geodesic_distances = np.zeros((len(self.vertex_indices), len(boundary_points)))

                # get distances
                for i, vertex in enumerate(self.vertex_indices):
                    for j, boundary_vertex in enumerate(boundary_points):
                        geodesic_distances[i, j] = shortest_path_length(
                            self._graph, source=vertex, target=boundary_vertex, weight="weight"
                        )

                # get distance variances
                dist_var = np.var(geodesic_distances, axis=1)

                # et the index of the min distance variance and store in centroid_index
                self._centroid_index = self.vertex_indices[np.argmin(dist_var)]
            sp.text = "Centroid Found!"
            sp.ok("✅")

        # return centroid_index
        return self._centroid_index

    def get_boundary_unordered(self) -> List[int]:
        # get nodes and edges
        nodes = self._vertex_indices
        edges = self._edges

        # find border edges
        # a border edge is an edge with at least one node not in `nodes`
        border_edges = list()
        for n1, n2 in edges:
            if not (n1 in nodes and n2 in nodes):
                border_edges.append((n1, n2))

        # from these border edges, find the nodes left
        # in the graph
        border_nodes = set()
        for n1, n2 in border_edges:
            if n1 in nodes:
                border_nodes.add(n1)
            elif n2 in nodes:
                border_nodes.add(n2)
        border_nodes = list(border_nodes)

        # get boundary edges, edges that
        # connect border node to any other border node
        boundary_edges = list()
        for n1, n2 in edges:
            if n1 in border_nodes and n2 in border_nodes:
                boundary_edges.append((n1, n2))

        return border_nodes

    def get_boundary(self) -> List[int]:
        """Returns vertex indices on boundary."""
        # get nodes and edges
        nodes = self._vertex_indices
        edges = self._edges

        # find border edges
        # a border edge is an edge with at least one node not in `nodes`
        border_edges = list()
        for n1, n2 in edges:
            if not (n1 in nodes and n2 in nodes):
                border_edges.append((n1, n2))

        # from these border edges, find the nodes left
        # in the graph
        border_nodes = set()
        for n1, n2 in border_edges:
            if n1 in nodes:
                border_nodes.add(n1)
            elif n2 in nodes:
                border_nodes.add(n2)
        border_nodes = list(border_nodes)

        # get boundary edges, edges that
        # connect border node to any other border node
        boundary_edges = list()
        for n1, n2 in edges:
            if n1 in border_nodes and n2 in border_nodes:
                boundary_edges.append((n1, n2))

        # # choose arbitrary border vertex to start off
        # # we need to loop this until we have a shortest path not <= 2 in length
        # # which can happen if the starting vertex is on a cycle
        # for i in range(len(border_nodes)):

        # create graph on border_edges
        g = Graph(boundary_edges)

        # get start vertex
        # start_vertex = border_nodes[i]
        start_vertex = border_nodes[0]

        # get neighbors of start_vertex and choose an
        # arbitrary end vertex
        end_vertex = [n for n in g.neighbors(start_vertex) if n in border_nodes][0]

        # remove the edge between end_vertex and start_vertex
        g.remove_edge(start_vertex, end_vertex)

        # create a dag
        dag = DiGraph()

        # initialize current_node to start and create a queue
        current_node = start_vertex
        q = Queue()

        # create set tracking history
        history_set = set()
        history_set.add(current_node)

        # loop until current_node is end_vertex
        while current_node != end_vertex:
            # add neighbors of current node to queue
            # and add edges to dag
            for n in g.neighbors(current_node):
                # only add neighbor to queue if not in history set
                # update the history set after adding
                if n not in history_set:
                    q.put(n)
                    history_set.add(n)
                    dag.add_edge(current_node, n)  # adds edge to dag

            # get next node to consider off queue
            current_node = q.get()

        # add end_vertex neighbors, if not in history_set
        for n in g.neighbors(end_vertex):
            if n not in history_set:
                dag.add_edge(end_vertex, n)

        # now get the longest path of the dag
        path = list(dag_longest_path(dag))

        # return path
        return path

    def get_boundary_path(self, counterclockwise: bool = True) -> List[int]:
        """Returns ordered list of vertices for path on boundary of ROI."""
        path = self.get_boundary()

        # check orientation of path, if clockwise reverse the path
        if self.compute_orientation(path) < 0:  # path is clockwise
            if counterclockwise:  # make path counterclockwise
                path.reverse()
        else:  # path is counterclockwise
            if not counterclockwise:  # make path clockwise
                path.reverse()

        # return the path
        return path

    def compute_orientation(self, path: List[int]) -> int:
        """Computes whether the path is clockwise or counter-clockwise."""
        # get centroid
        centroid = self.gifti.vertices[self.centroid_index]

        # get centroid surface normal
        cenroid_surf_norm = self.gifti.vertex_normals[self.centroid_index]

        # get vertices
        vertices = self.gifti.vertex_indices_to_positions(path)

        # get vertex normals
        vertex_normals = self.gifti.vertex_normals[path]

        # now compute the cross-product of adjacent vectors along the pass
        dot_vecs = np.zeros((self.gifti.num_vertices,))
        for i, (vertex_a, vertex_b, normal) in enumerate(zip(np.roll(vertices, 1), vertices, vertex_normals)):
            # get the vectors
            vec_ac = vertex_a - centroid
            vec_bc = vertex_b - centroid

            # compute the cross product and normalize to unit norm
            cross_vec = np.cross(vec_bc, vec_ac)
            cross_vec /= np.linalg.norm(cross_vec)

            # dot this with the vertex normal at b
            dot_vecs[i] = np.dot(cross_vec, normal)

        # return sum of the dot vecs
        return np.sum(dot_vecs)

    def get_spiral_path(
        self,
        counterclockwise: bool = True,
        cycles: float = 2,
        interpolate: bool = False,
        add_reverse_path: bool = False,
    ) -> List[int]:
        """Get a spiral path on the ROI"""
        # get centroid index
        centroid_index = self.centroid_index

        # get boundary path
        boundary_path = self.get_boundary_path(counterclockwise)

        # get paths and path lengths for each point on boundary path
        centroid_boundary = list()
        centroid_boundary_distance = list()
        for vertex_idx in boundary_path:
            centroid_boundary.append(shortest_path(self._graph, source=centroid_index, target=vertex_idx))
            centroid_boundary_distance.append(
                shortest_path_length(self._graph, source=centroid_index, target=vertex_idx)
            )

        # from the number of cycles, figure out the shrink rate of r
        number_of_checkpoints = int(len(boundary_path) * cycles)
        delta_r = 1.0 / number_of_checkpoints

        # traverse spiral path
        path = list()
        path.append(boundary_path[-1])  # add last point adjacent to first point
        current_r = 1.0
        for idx in range(number_of_checkpoints):
            # make sure idx is in range of number of boundary paths
            idx = idx % len(boundary_path)

            # get path at angle
            theta_path = centroid_boundary[idx]

            # get max distance at angle
            theta_max_dist = centroid_boundary_distance[idx]

            # find the index for the current r
            r_idx = round(current_r * theta_max_dist)

            # get the point along theta
            point = theta_path[r_idx]

            # interpolate
            if interpolate:
                # find the path between the previous point and the new point
                # exclude first since it's already on path
                interpath = shortest_path(self._graph, source=path[-1], target=point)[1:]
                path.extend(interpath)
            else:
                path.append(point)

            # update current_r
            current_r -= delta_r

            # end prematurely when r_idx <= 1
            if r_idx <= 1:
                break

        # go from last point to centroid
        interpath = shortest_path(self._graph, source=path[-1], target=centroid_index)[1:]
        path.extend(interpath)

        # remove duplicate subsequent points due to interpolation
        new_path = list()
        last_point = None
        for p in path:
            if last_point == p:
                continue
            else:
                new_path.append(p)
                last_point = p

        # if add reverse path if set
        if add_reverse_path:
            # make copy of new_path and reverse it
            new_path_reverse = new_path.copy()
            new_path_reverse.reverse()

            # extend new_path with new_path_reverse (removing the first point)
            new_path.extend(new_path_reverse[1:])

        # return the path
        return new_path
