from concurrent.futures import as_completed, ProcessPoolExecutor
from pathlib import Path
from os import symlink
from tempfile import TemporaryDirectory
from typing import List, Tuple, Union

from yaspin import yaspin
from yaspin.spinners import Spinners

from wbsurfer.border import Border
from wbsurfer.images import Cifti
from wbsurfer.scene import Scene
from wbsurfer.utils import run_ffmpeg


class Movie:
    """Object representing a movie of scenes."""

    def __init__(
        self,
        scene_file: str,
        scene_name: str,
        border_file: str = None,
        vertices_file: str = None,
        vertices: List[str] = None,
        cifti_row_file: str = None,
        cifti_row: List[int] = None,
        timepoints: List[int] = None,
    ):
        # save scene_file and scene_name
        self.scene_file = scene_file
        self.scene_name = scene_name

        # get the scene
        self.scene = Scene(scene_file, scene_name, check_valid=True)

        # initialize timepoints
        self.timepoints = None

        # get the border
        if border_file:
            self.border = Border.load(border_file)
        elif vertices_file or vertices:
            if vertices:
                surface_name = vertices[0]
                vertices = [int(i) for i in vertices[1:]]
            else:
                vertices, surface_name = self._load_txt(vertices_file, header=True)
            self.border = Border(surface_name, len(vertices))
            self.border.set_vertices(vertices)
        elif cifti_row_file or cifti_row:
            if cifti_row:
                row_indices = cifti_row
            else:
                row_indices = self._load_txt(cifti_row_file)
            vertices = list()
            current_surface = None
            for r in row_indices:
                vertex, surface_name = self.scene._row_to_vertex_index(r)
                vertices.append(vertex)
                if current_surface is not None:
                    assert (
                        current_surface == surface_name
                    ), "The row indices you have provided are on different surfaces!"
                current_surface = surface_name
            self.border = Border(surface_name, len(vertices))
            self.border.set_vertices(vertices)
        elif timepoints:
            self.timepoints = timepoints

    @staticmethod
    def _load_txt(filename: str, header: bool = False) -> Union[List[int], Tuple[List[int], str]]:
        """Loads a line seperated text file with integers.

        Parameters
        ----------
        filename : str
            Path to text gile to load
        header : bool, optional
            Flag to specify whether to read a string header, by default False

        Returns
        -------
        Union[List[int], Tuple[List[int], str]]
            Returns a list of integers or a tuple containing a list of integers and a string for the header.
        """
        with open(filename, "r") as f:
            lines = f.readlines()
            if header:
                header_txt = lines[0].rstrip()
                int_list = [int(i.rstrip()) for i in lines[1:] if i.rstrip() != ""]
                return (int_list, header_txt)
            return [int(i.rstrip()) for i in lines if i.rstrip() != ""]

    @staticmethod
    def _process_scene(
        path: str, vertex: int, surface: str, scene_file: str, scene_name: str, width: int, height: int, index: int
    ) -> Tuple[int, str]:
        # load scene
        scene = Scene(scene_file, scene_name)

        # write scene image to path
        scene.write_img_vertex(vertex, surface, path, width=width, height=height, index=index)

        # return the vertex and surface that was processed
        return (vertex, surface)

    @staticmethod
    def _process_scene_time(
        path: str, timepoint: int, scene_file: str, scene_name: str, width: int, height: int, index: int
    ) -> None:
        # load scene
        scene = Scene(scene_file, scene_name)

        # change scene timepoint index
        scene.set_time_index(timepoint)

        # save scene file to path
        path = Path(path)
        file_prefix = f"{index:09d}" if index is not None else f"{timepoint:09d}"
        filename = (path / (file_prefix + ".png")).as_posix()
        scene.write_img(filename, width, height)

        # return something
        return (timepoint, "dtseries")

    def generate_movie(
        self,
        output_file: str,
        framerate: int = 15,
        encoding: str = "libx265",
        num_threads: int = 8,
        method: str = "chenhan",
        skip_interp: bool = False,
        loops: int = 1,
        width: int = 1920,
        height: int = 1080,
    ) -> None:
        """Generates a movie by looping over the points specified in the border file."""
        # get the vertices and surface
        if self.timepoints:
            surface_name = "dtseries"
        else:
            if skip_interp:  # skip interpolation
                vertices, surface_name = self.border.vertices
            else:
                vertices, surface_name = self.border.get_interpolated_vertices(
                    self.scene.cifti, self.scene.get_surf(self.border.surface_name), method
                )
            print(f"Processing vertices: {vertices} from surface: {surface_name}")

        # create temppath for intermediate outputs (not on /tmp bc it can run out of space!)
        temppath = Path(output_file).parent
        temppath.mkdir(exist_ok=True)

        # start a temp directory at temppath
        with TemporaryDirectory(dir=temppath.as_posix()) as tempdir:
            # create a process pool
            with ProcessPoolExecutor(max_workers=num_threads) as executor:
                results = list()
                if self.timepoints:
                    for frameindex, timepoint in enumerate(self.timepoints):
                        results.append(
                            executor.submit(
                                self._process_scene_time,
                                tempdir,
                                timepoint,
                                self.scene_file,
                                self.scene_name,
                                width,
                                height,
                                frameindex,
                            )
                        )
                else:
                    for frameindex, vertex in enumerate(vertices):
                        results.append(
                            executor.submit(
                                self._process_scene,
                                tempdir,
                                vertex,
                                surface_name,
                                self.scene_file,
                                self.scene_name,
                                width,
                                height,
                                frameindex,
                            )
                        )

                # get future results as they complete
                with yaspin(Spinners.dots12, text="(っ- ‸ – ς) Processing scenes...", color="blue") as sp:
                    for r in as_completed(results):
                        vertex, _ = r.result()
                        sp.write("Finished processing scene: %s, vertex:  %s" % (surface_name, vertex))
                    sp.text = "Finished processing scenes."
                    sp.ok("✅")

            # if loops > 1, duplicate the processed images
            if loops > 1:
                for loop in range(1, loops):
                    frameindex = len(vertices) * loop
                    for index in range(len(vertices)):
                        # get the base image to symlink
                        base_img = f"{index:09d}.png"
                        # create path to duplicate
                        dup_img = (Path(tempdir) / f"{frameindex:09d}.png").as_posix()
                        # symlink to base image
                        symlink(base_img, dup_img)
                        # increment the frameindex
                        frameindex += 1

            # setup input images paths
            in_images = (Path(tempdir) / "%09d.png").as_posix()

            # run ffmpeg to encode images into movie
            run_ffmpeg(
                f"-hide_banner -y -r {framerate} -start_number 0 -i {in_images} -c:v {encoding} -r "
                f"{framerate} -pix_fmt yuv420p {output_file}"
            )

            print("(^o^)~≪☆✅ Movie sucessfully encoded! ✅☆≫~(^o^)／")
