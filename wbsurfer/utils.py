import subprocess


def run_ffmpeg(flags: str, capture_output=False) -> None:
    """Runs ffmpeg utility"""
    subprocess.run(f"ffmpeg {flags}", shell=True, check=True, capture_output=capture_output)


def run_wbcommand_show_scene(
    scene_file: str,
    scene_name: str,
    output_img: str,
    width: int = 1920,
    height: int = 1080,
    capture_output: bool = False,
) -> None:
    """Runs wb_command -show-scene"""
    subprocess.run(
        f"OMP_NUM_THREADS=1 wb_command -show-scene {scene_file} {scene_name} {output_img} {width} {height}",
        shell=True,
        check=True,
        capture_output=capture_output,
    )
