import argparse

# import json
# import cProfile

from scipy.io import savemat
from wbsurfer.search import isearch


def main() -> None:
    parser = argparse.ArgumentParser(
        description="Interdigitation Search.",
        epilog="Author: Andrew Van, vanandrew@wustl.edu, 02/02/2022",
    )
    parser.add_argument("output_file", help="Output file.")
    parser.add_argument("cifti_file", help="Cifti file.")
    parser.add_argument("gifti_files", nargs="+", help="Surface file.")
    checkpoints = parser.add_mutually_exclusive_group()
    checkpoints.add_argument(
        "-p",
        "--vertices_file",
        help="Line seperated list of vertex indices to search. These should be zero indexed. "
        "The very first line of this file should list the surface these vertices are defined on (either CORTEX_LEFT or "
        "CORTEX_RIGHT)",
    )
    checkpoints.add_argument(
        "-o",
        "--vertex_indices",
        nargs="+",
        help="Same as `--vertices_file`, but direct command line entry (e.g. -o CORTEX_LEFT 900 901 902). ",
    )
    checkpoints.add_argument(
        "-c",
        "--cifti_row_file",
        help="Line seperated list of cifti row indices to search. These should be zero indexed "
        "(In other words, you should subtract 1 from the index that you see in connectome workbench UI).",
    )
    checkpoints.add_argument(
        "-x",
        "--cifti_row",
        type=int,
        nargs="+",
        help="Same as `--cifti_row_file`, but direct command line entry (e.g. -x 15071 16702 17403).",
    )
    parser.add_argument("-r", "--radius", default=20.0, type=float, help="Radius of cross-section (Default: 20.0).")
    parser.add_argument(
        "-a",
        "--min_angle",
        default=10.0,
        type=float,
        help="Minimum angle of separation for each cross-section in degrees (Default: 10.0). "
        "Note that this is computed by estimating the arc-length separation between the last points "
        "of each cross-section by the euclidean distance, which is probably accurate enough for "
        "small angles, but can overestimate the separation when the angle is large.",
    )
    parser.add_argument(
        "-t",
        "--tolerance",
        default=0.5,
        type=float,
        help="Tolerance value for radius, defined as the radius +/- tolerance. (Default: 0.5)",
    )
    parser.add_argument("-n", "--nproc", type=int, default=4, help="Number of processes to use. (Default: 4)")
    parser.add_argument("--debug", action="store_true", help="For debugging.")

    # get arguments
    args = parser.parse_args()

    # get inputs dictionary
    dargs = vars(args)

    # run isearch
    # cProfile.run(
    #     "from wbsurfer.search import isearch;"
    #     'isearch('
    #     '"TBIPFM01_50440_20210929_b27_MNI152_T1_2mm_LR_surf_subcort_32k_fsLR_brainstem_smooth1.7.dtseries.nii",'
    #     '["sub-TBIPFM01.R.inflated.32k_fs_LR.surf.gii"],vertex_indices=["CORTEX_RIGHT","7278"])',
    #     sort="tottime"
    # )
    cross_section_dict = isearch(**vars(args))

    # combine infrotion from
    combined_cross_section_list = list()
    for key in cross_section_dict:
        combined_cross_section_list.extend(cross_section_dict[key])

    # save information into mat file
    savemat(args.output_file, {"cross_section_data": combined_cross_section_list})

    # save dict to JSON (for debugging) TODO: expose to frontend
    # with open(args.output_file, "w") as f:
    #     json.dump(cross_section_dict, f, indent=2, separators=(",", ": "))
