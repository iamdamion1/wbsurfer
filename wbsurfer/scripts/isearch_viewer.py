import argparse

import numpy as np
from scipy.io import loadmat

from wbsurfer.images import Cifti, Gifti


def main() -> None:
    parser = argparse.ArgumentParser(description="Debug viewer for results of isearch. Must have pyrender installed!")
    parser.add_argument("mat_file", help="output mat file")
    parser.add_argument("cifti_file", help="cifti file")
    parser.add_argument("gifti_file", help="surface file")
    checkpoints = parser.add_mutually_exclusive_group()
    checkpoints.add_argument(
        "-o", "--vertex_index", nargs=2, metavar=("SURFACE_NAME", "VERTEX_INDEX"), help="surface/vertex to display."
    )
    checkpoints.add_argument("-x", "--cifti_row", type=int, help="cifti row to display.")
    parser.add_argument("-i", "--cross_section_index", type=int, help="cross section index to display (zero-indexed)")
    parser.add_argument("--no_view", action="store_true", help="only report info in mat file.")
    parser.add_argument("--debug", action="store_true", help="For debugging.")

    # parse arguments
    args = parser.parse_args()

    # load mat file and restructure into a python dict
    cross_section_data = loadmat(args.mat_file)["cross_section_data"][0]
    print("Loaded cross sections with available rows: %s" % [d[0, 0][0][0, 0] for d in cross_section_data])

    # load cifti/gifti
    cifti = Cifti(args.cifti_file)
    gifti = Gifti(args.gifti_file)

    # create masked gifti
    masked_gifti = gifti.mask(cifti)

    # parse vertex to display
    _, offset, _ = cifti.get_vrtable(gifti.name)
    if args.vertex_index:
        row_index = cifti.vertex_to_row_index(int(args.vertex_index[1]), args.vertex_index[0])
    else:
        row_index = args.cifti_row

    # return if row_index is not set
    if row_index is None:
        return

    # get cross sections for selected index
    data = [i[0, 0] for i in cross_section_data if i[0, 0][0] == row_index][0]
    if args.debug:
        cross_sections = data[4][0]
    else:
        cross_sections = data[3][0]
    print("Available cross sections: %s" % list(range(len(cross_sections))))

    # get masked_vertex index
    masked_vertex_index = row_index - offset

    # convert cross section rows to masked vertices
    cross_sections = [c.ravel() - offset for c in cross_sections]

    # get point representation of cross sections
    if args.debug:
        cross_section_points = [c.reshape(-1, 3) for c in cross_sections]
    else:
        cross_section_points = [masked_gifti.vertices[c] for c in cross_sections]

    # get cross section ineex if defined
    if args.cross_section_index is not None:
        cross_section_points = [
            cross_section_points[args.cross_section_index],
        ]
        print("Selected cross section: %d" % args.cross_section_index)

    # append chosen vertex
    cross_section_points.append(masked_gifti.vertices[masked_vertex_index])

    # create colors and sizes
    colors = [np.random.rand(3).tolist() for _ in cross_section_points[:-1]]
    colors.append([0, 0, 255])
    sizes = [0.3 for _ in cross_section_points[:-1]]
    sizes.append(0.5)

    # render all cross_sections on surface for chosen index
    if not args.no_view:
        masked_gifti.render(cross_section_points, colors, sizes)
