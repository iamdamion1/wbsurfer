import argparse

from wbsurfer import Movie


def main() -> None:
    # command prompt
    parser = argparse.ArgumentParser(
        description="Generates a surface path trace movie on a workbench scene.",
        epilog="Author: Andrew Van, vanandrew@wustl.edu, 11/11/2021",
    )
    parser.add_argument("scene_file", help="Base scene file to generate movie on.")
    parser.add_argument("scene_name", help="Name of scene in scene file to display.")
    checkpoints = parser.add_mutually_exclusive_group(required=True)
    checkpoints.add_argument(
        "-b",
        "--border_file",
        help="A border file (or alternatively a .gii) with defined path of vertices to display. "
        "NOTE: If a .gii file is used, wb_surfer assumes the vertex ordering since the .gii file does not store vertex "
        "ordering info (it only tells you the location of the border vertices). Consequently, wb_surfer just starts "
        "at an arbitrary vertex (the lowest indexed vertex) and loops through the next closest vertex until reaching "
        "the end. This can result in path traces that you did not intend. TLDR: Use a .border file for more consistent "
        "behavior.",
    )
    checkpoints.add_argument(
        "-p",
        "--vertices_file",
        help="Line seperated list of vertex indices to use for the path. These should be zero indexed. "
        "The very first line of this file should list the surface these vertices are defined on (either CORTEX_LEFT or "
        "CORTEX_RIGHT)",
    )
    checkpoints.add_argument(
        "-o",
        "--vertices",
        nargs="+",
        help="Same as `--vertices_file`, but direct command line entry (e.g. -o CORTEX_LEFT 900 901 902). ",
    )
    checkpoints.add_argument(
        "-c",
        "--cifti_row_file",
        help="Line seperated list of cifti row indices to use for the path. These should be zero indexed "
        "(In other words, you should subtract 1 from the index that you see in connectome workbench UI).",
    )
    checkpoints.add_argument(
        "-x",
        "--cifti_row",
        type=int,
        nargs="+",
        help="Same as `--cifti_row_file`, but direct command line entry (e.g. -x 15071 16702 17403).",
    )
    checkpoints.add_argument(
        "-t", "--timepoints", type=int, nargs="+", help="For dtseries, change the index of the time series."
    )
    parser.add_argument("output_file", help="Output video file to save (add the .mp4 extension please!).")
    parser.add_argument(
        "-e",
        "--encoding",
        default="libx264",
        help="Encoder to use (default: libx264). Any valid ffmpeg encoder can be used here.",
    )
    parser.add_argument(
        "-r", "--framerate", type=int, default=15, help="Rate at which scenes are displayed (default: 15)."
    )
    parser.add_argument(
        "-l", "--loops", type=int, default=1, help="Number of times to loop through the path (default: 1)."
    )
    parser.add_argument(
        "-w",
        "--window_size",
        type=int,
        nargs=2,
        default=[1920, 1080],
        metavar=("WIDTH", "HEIGHT"),
        help="Sets the window size of the renderer.",
    )
    parser.add_argument(
        "-m",
        "--method",
        choices=["dijkstra", "chenhan"],
        default="chenhan",
        help="Method for interpolating path (Default: chenhan).",
    )
    parser.add_argument("--skip_interp", action="store_true", help="Skips interpolation between border points.")
    parser.add_argument(
        "-n",
        "--num_threads",
        type=int,
        default=8,
        help="Number of threads to allocate for scene processing. "
        "Note that this only affects SCENE processing since ffmpeg can autoscale the multithreading of "
        "its processing perfectly fine on its own.",
    )

    # get arguments
    args = parser.parse_args()

    # create Movie object
    m = Movie(
        args.scene_file,
        args.scene_name,
        args.border_file,
        args.vertices_file,
        args.vertices,
        args.cifti_row_file,
        args.cifti_row,
        args.timepoints,
    )

    # generate movie
    m.generate_movie(
        args.output_file,
        args.framerate,
        args.encoding,
        args.num_threads,
        args.method,
        args.skip_interp,
        args.loops,
        width=args.window_size[0],
        height=args.window_size[1],
    )
