import argparse

from wbsurfer.border import Border
from wbsurfer.images import Cifti, Gifti


def main() -> None:
    parser = argparse.ArgumentParser(
        description="Generates a border file for a specific ROI.",
        epilog="Author: Andrew Van, vanandrew@wustl.edu, 10/28/2021",
    )
    parser.add_argument("cifti_file", help="ROI file.")
    parser.add_argument("gifti_file", help="Surface file.")
    parser.add_argument("output_file", help="Border file output.")
    parser.add_argument("-i", "--index", type=int, default=1, help="Index of ROI to output.")
    parser.add_argument("-n", "--interpolate", action="store_true", help="Interpolation")
    parser.add_argument("-c", "--clockwise", action="store_true", help="Sets rotation clockwise.")
    parser.add_argument("-r", "--cycles", type=float, default=1, help="Number of spiral cycles.")
    parser.add_argument("-t", "--type", default="spiral", choices=["spiral", "boundary"], help="Path type.")
    parser.add_argument("-l", "--reverse", action="store_true", help="Appends reverse path if type is spiral.")

    # get arguments
    args = parser.parse_args()

    # load Cifti/Gifti
    cifti = Cifti(args.cifti_file)
    gifti = Gifti(args.gifti_file)

    # create Border
    border = Border(gifti.name, gifti.num_vertices)

    # generate path on border file
    border.generate_path(
        cifti, gifti, args.index, not args.clockwise, args.cycles, args.interpolate, args.type, args.reverse
    )

    # write border file
    border.write(args.output_file)
