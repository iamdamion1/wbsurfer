"""This module contains search algorithsm for finding areas of interdigitation."""
from __future__ import annotations
from os.path import basename
from typing import Dict, List, Tuple, Union
from itertools import repeat
from contextlib import nullcontext
from concurrent.futures import ProcessPoolExecutor

import numpy as np
from networkx.algorithms.shortest_paths import shortest_path
from wbsurfer.geodesic import GeodesicPath
from wbsurfer.images import Cifti, Gifti
from wbsurfer.movie import Movie
from yaspin import yaspin
from yaspin.spinners import Spinners


# create random number generator
rng = np.random.default_rng(77)


def draw_line(path: np.ndarray, step_size: float = 0.025) -> np.ndarry:
    line = list()
    number_of_iterations = int(1 / step_size)
    for b, e in zip(path[:-1], path[1:]):
        subline = [b]
        for i in range(1, number_of_iterations):
            step = i * step_size
            subline.append(e * step + b * (1 - step))
        subline.append(e)
        subline = np.stack(subline)
        line.append(subline)
    line = np.concatenate(line)
    return line


# A code snippet for debugging
#
# if self.target == SOME_TARGET_HERE:
#     gifti = Gifti("./sub-TBIPFM01.L.midthickness.32k_fs_LR.surf.gii")
#     verts = vertices[np.unique(pfaces)]
#     fces = np.searchsorted(np.unique(pfaces), pfaces)
#     gifti.agg_data = (verts, fces)
#     from trimesh import Trimesh
#     gifti.mesh = Trimesh(verts, fces, process=False)
#     gifti.render(
#         [
#             # draw_line(qline_seg),
#             draw_line(np.array([source_vertex, self.previous_point])),
#             draw_line(np.array([source_vertex, source_vertex + 3 * vedge_vec]))
#         ]
#     )


class InterdigitationSearch:
    """Searches for areas of interdigitation on brain."""

    @classmethod
    def create(cls, ciftiname: str, giftiname: str, *args, **kwargs) -> InterdigitationSearch:
        cifti = Cifti(ciftiname)
        gifti = Gifti(giftiname)
        return cls(cifti, gifti, *args, **kwargs)

    def __init__(
        self,
        cifti: Cifti,
        gifti: Gifti,
        vertices: np.ndarray = None,
        faces: np.ndarray = None,
        face_normals: np.ndarray = None,
        deleted_faces: np.ndarray = None,
    ):
        # get the masked gifti
        gifti = gifti.mask(cifti)

        # grab the vertices and faces for the surface
        if vertices is not None:
            self.vertices = vertices
        else:
            self.vertices = gifti.vertices
        if faces is not None:
            self.faces = faces
        else:
            self.faces = gifti.faces
        if face_normals is not None:
            self.face_normals = face_normals
        else:
            self.face_normals = gifti.face_normals
        if deleted_faces is not None:
            self.deleted_faces = deleted_faces
        else:
            self.deleted_faces = np.array([[-1, -1, -1], [-2, -2, -2]])

        # get graph of vertices
        self.vertex_graph = gifti.mesh.vertex_adjacency_graph

        # create object to search geodesic paths
        self.geodesic_search = GeodesicPath(self.vertices, self.faces)

    @staticmethod
    def compute_normal_vector(v0: np.ndarray, v1: np.ndarray) -> np.ndarray:
        vec = v0 - v1
        return vec / np.linalg.norm(vec)

    def search(
        self, source: int, radius: float = 20.0, min_angle: float = 10, tolerance: float = 0.5, nospinner: bool = False
    ) -> Tuple[List[List[int]], List[np.ndarray], List[Union[int, Tuple[int]]]]:
        # get distances for vertex
        distances = self.geodesic_search.distances(source)

        # get all target vertices within radius
        targets = np.argwhere((distances > radius - tolerance) & (distances < radius + tolerance)).ravel()

        # shuffle the targets so that we have some variance in the cross sections
        # from vertex to vertex
        rng.shuffle(targets)

        # get the target positions
        target_positions = self.vertices[targets]

        # prune paths by minimum angle
        # compute minimum angle as a distance
        min_distance = 2 * radius * np.pi * (min_angle / 360.0)
        # loop through each target position and delete entries that violate min distance
        i = 0  # track index of current position
        while i < target_positions.shape[0]:
            pos = target_positions[i]  # initialize position
            # get the distance from current to other positions
            dists = np.linalg.norm(target_positions - pos, axis=1)
            # find positions that are below min distance
            delete_indices = np.argwhere(dists < min_distance).ravel()
            # remove current and already considered indices
            delete_indices = delete_indices[delete_indices > i]
            # make mask
            keep_mask = np.ones(target_positions.shape[0], dtype=bool)
            keep_mask[delete_indices] = False
            # delete indices below min distance
            target_positions = target_positions[keep_mask]
            targets = targets[keep_mask]
            # increment index
            i += 1

        # loop over all targets
        position_list = list()  # list of positions for cross section
        path_list = list()  # list of proportion/left vertex/right vertex on edge
        if nospinner:
            cm = nullcontext()
        else:
            cm = yaspin(Spinners.dots12, text="Generating cross section...", color="blue")
        with cm as sp:
            for target in targets:
                if not nospinner:
                    sp.text = "Generating cross section to vertex %s" % target
                path, pos_path = self.get_cross_section(source, target, radius, tolerance)
                if pos_path.shape[0] != 0:
                    position_list.append(pos_path)
                    path_list.append(path)
            if not nospinner:
                sp.text = "Cross sections generated."
                sp.ok("✅")

        # do a second pruning
        # this is because the original targets that were nearly 180 degrees across from
        # each other cannot be found until after cross sections are generated
        endpoints = np.array([np.array([pl[0], pl[-1]]) for pl in position_list])  # for each path grab it's endpoints
        # set initial index
        i = 0
        # get the position of the source point
        source_pos = self.vertices[source]
        # get indices of cross-sections to skip
        # these are mainly due to vertices near the medial wall
        # where the pruning calculation will be off since the cross-section
        # doesn't reach it's full length
        skip_cs = np.unique(np.argwhere(np.linalg.norm(endpoints - source_pos, axis=2) < radius - tolerance)[:, 0])
        while i < endpoints.shape[0]:
            # get current endpoints
            pos0 = endpoints[i, 0]
            pos1 = endpoints[i, 1]
            # compare to every other endpoint
            dist0 = np.linalg.norm(endpoints - pos0, axis=2)
            dist1 = np.linalg.norm(endpoints - pos1, axis=2)
            # get indices for deletion
            delete_indices0 = np.argwhere(dist0 < min_distance)[:, 0]
            delete_indices1 = np.argwhere(dist1 < min_distance)[:, 0]
            delete_indices = np.union1d(delete_indices0, delete_indices1)
            # remove from deletion indices that we want to skip
            delete_indices = delete_indices[~np.in1d(delete_indices, skip_cs)]
            # remove current and already considered indices
            delete_indices = delete_indices[delete_indices > i]
            # make mask
            keep_mask = np.ones(endpoints.shape[0], dtype=bool)
            keep_mask[delete_indices] = False
            # delete indices below min distance
            endpoints = endpoints[keep_mask]
            position_list = [pl for i, pl in enumerate(position_list) if i not in delete_indices]
            path_list = [pl for i, pl in enumerate(path_list) if i not in delete_indices]
            # increment index
            i += 1

        # interpolate each path to the nearest vertices
        interpolated_paths = list()
        for geodesic_path in path_list:
            # construction function for returning nearsest vertex
            def nearest_vertex(x):
                if type(x) is not tuple:  # on vertex
                    return x
                else:  # on edge
                    if x[0] >= 0.5:  # return right vertex
                        return x[1]
                    else:  # return left vertex
                        return x[2]

            nearest_vertex_path = [nearest_vertex(v) for v in geodesic_path]

            # remove duplicates from interpolated path
            last_vertex = -1
            nodups_interp_path = list()
            for i in nearest_vertex_path:
                if i != last_vertex:
                    nodups_interp_path.append(i)
                    last_vertex = i

            # make sure path is continuous on vertex graph
            final_interpolated_path = list()
            for v1, v2 in zip(nodups_interp_path[:-1], nodups_interp_path[1:]):
                path = shortest_path(self.vertex_graph, source=v1, target=v2)
                final_interpolated_path.extend(path[:-1])

            # vertex graph may have added duplicates, go through and remove them
            already_traversed = set()
            final_final_interpolated_path = list()
            for i in final_interpolated_path:
                if i not in already_traversed:
                    final_final_interpolated_path.append(i)
                    already_traversed.add(i)
                else:  # remove points until back at where the duplicate was
                    while i != final_final_interpolated_path[-1]:
                        final_final_interpolated_path = final_final_interpolated_path[:-1]

            # add to interpolated paths list
            interpolated_paths.append(final_final_interpolated_path)

        # return cross section info for source
        return (interpolated_paths, position_list, path_list)

    def get_cross_section(
        self, source: int, target: int, radius: float, tolerance: float
    ) -> Tuple[List[Union[int, Tuple[int]]], np.ndarray]:
        path = self.geodesic_search.path(source, target)

        # get edges
        edges = self.geodesic_search.as_edges(path)

        # construct edge/vertex path
        ev_path = list()
        for p, e in zip(path, edges):
            if p.isVertex:
                ev_path.append(p.index)
            else:
                ev_path.append((p.proportion, e.indexOfRightVert, e.indexOfLeftVert))

        # now we find the path in the opposite direction

        # get the vector from path neighbor to source point
        pos = self.geodesic_search.as_vertex_positions(path)
        # make vector from previous vertex to current vertex
        self.previous_point = pos[1]
        nvec = self.compute_normal_vector(pos[0], pos[1])

        # get the previous edge of the path
        if isinstance(ev_path[1], tuple):
            edge = ev_path[1][1:]
        else:
            # the previous edge is on a vertex so
            # we need to define an edge for this vertex
            faces = self.faces
            # get faces where vertex is located
            pfaces = faces[np.argwhere(faces == ev_path[1])[:, 0]]
            # get the face where source is on
            for face in pfaces:
                if source in face:
                    break
            # now get the edge from this face, that doesn't contain the source
            edge = face.tolist()
            edge.remove(source)

        # set current point
        point_index = path[0].index
        point = self.vertices[point_index]
        proportion = 0  # since the first point is always a vertex
        current_distance = 0.0

        opos = list()
        oev_path = list()
        # go defined radius within tolerance
        while current_distance < (radius - tolerance):
            # save the current point
            opoint = point
            # check the current proportion, if close to 0 or 1, then this point is a vertex
            if np.isclose(proportion, 0.0) or np.isclose(proportion, 1.0):
                # get next point from vertex
                self.target = target
                proportion, edge, point, point_index, face, nvec = self.get_next_point_from_vertex(
                    nvec, point_index, edge
                )
            else:
                # get next point from edge
                proportion, edge, point, point_index, face, nvec = self.get_next_point_from_edge(
                    nvec, point, edge, face
                )
            # if next point invalid, return empty arrays
            # it means this path is at the border
            # UPDATE: leave in this cross section (Evan requested)
            if edge[0] == -1 and edge[1] == -1:
                break
            # add point to path
            opos.append(point)
            # update oev_path, if vertex add the index, if edge add proportion and edge
            if np.isclose(proportion, 0.0) or np.isclose(proportion, 1.0):
                oev_path.append(point_index)
            else:
                oev_path.append((proportion, edge[0], edge[1]))

            # compute the distance and update
            current_distance += np.linalg.norm(point - opoint)
        # stack to numpy array
        if len(opos) > 0:
            opos = np.stack(opos, axis=0)
            cpos = np.concatenate((np.flip(opos, axis=0), pos), axis=0)
        else:
            cpos = pos

        # combine paths
        oev_path.reverse()
        cev_path = oev_path + ev_path

        # return paths
        return cev_path, cpos

    @staticmethod
    def rotate_vector(d: np.ndarray, current_normal: np.ndarray, new_normal: np.ndarray) -> np.ndarray:
        # See the following:
        # https://math.stackexchange.com/questions/2292895/walking-on-the-surface-of-a-triangular-mesh
        a = np.cross(current_normal, new_normal)
        a /= np.linalg.norm(a)
        sinphi = np.linalg.norm(np.cross(current_normal, new_normal))
        cosphi = np.dot(current_normal, new_normal)
        return d * cosphi + np.cross(a, d) * sinphi + a * np.dot(a, d) * (1 - cosphi)

    def get_next_point_from_edge(
        self, nvec: np.ndarray, source_vertex: np.ndarray, source_edge: List[int], previous_face: np.ndarray
    ) -> Tuple[float, List[int], np.ndarray, int, np.ndarray, np.ndarray]:
        vertices = self.vertices
        faces = self.faces

        # get the potential faces from this source edge
        faces_index = np.argwhere(faces == source_edge[0])[:, 0]
        pfaces = faces[faces_index]
        pfaces_index = np.argwhere(pfaces == source_edge[1])[:, 0]
        pfaces = pfaces[pfaces_index]

        # get the new face on the source edge
        cface = None
        for i, face in enumerate(pfaces):
            # get the face that doesn't intersect with the previous face
            if np.intersect1d(face, previous_face).shape[0] != 3:
                cface = face
                cface_index = faces_index[pfaces_index[i]]
            elif np.intersect1d(face, previous_face).shape[0] == 3:  # this is the previous face
                previous_face_index = faces_index[pfaces_index[i]]

        # if we couldn't find a new face, return
        if cface is None:
            return (0.0, [-1, -1], np.array([]), 0, np.array([]), np.ndarray([0, 0, 0]))

        # compute the normal of the previous and new faces
        cnormal = self.face_normals[previous_face_index]
        nnormal = self.face_normals[cface_index]

        # rotate the nvec onto the new face
        q = self.rotate_vector(nvec, cnormal, nnormal)

        # get the potential line segments for intersection
        # first find the vertex not on the source edge
        for nv in cface:
            if nv not in source_edge:
                break
        # now create line segments to this vertex
        lsi = list()
        ls = list()
        for v in source_edge:
            lsi.append([v, nv])
            ls.append(vertices[np.array([v, nv])])

        # now create a line segment for the q vector
        end_vertex = 5 * q + source_vertex
        qls = np.array([end_vertex, source_vertex])

        # now find the potential intersection between the q vector line segment
        # and each edge
        for line_seg, edge in zip(ls, lsi):
            proportion, pt = self.intersection(line_seg, qls)
            if pt is not False:
                # update index to use point closest to proportion
                if proportion >= 0.5:
                    index = edge[0]
                else:
                    index = edge[1]
                return (proportion, edge, pt, index, np.array(cface), q)

        # if we got here, we couldn't find a intersecting edge
        return (0.0, [-1, -1], np.array([]), 0, np.array([]), np.ndarray([0, 0, 0]))

    def get_next_point_from_vertex(
        self, nvec: np.ndarray, source_index: int, previous_edge: List[int]
    ) -> Tuple[float, List[int], np.ndarray, int, np.ndarray, np.ndarray]:
        vertices = self.vertices
        faces = self.faces

        # get potential faces
        pfaces_index = np.argwhere(faces == source_index)[:, 0]
        pfaces = faces[pfaces_index]

        # remove face where path has already traversed
        for i, p in enumerate(pfaces):
            if previous_edge[0] in p and previous_edge[1] in p:
                # save current face
                cface = pfaces[i]
                cface_index = pfaces_index[i]
                pfaces = np.delete(pfaces, i, axis=0)
                pfaces_index = np.delete(pfaces_index, i, axis=0)
                break

        # get face normals
        cnormal = self.face_normals[cface_index]
        normals = self.face_normals[pfaces_index]

        # rotate the nvec onto each face
        q = list()
        for i, nnormal in enumerate(normals):
            q.append(self.rotate_vector(nvec, cnormal, nnormal))
        q = np.array(q)

        # now get the potential line segments where each q vector should intersect
        ls = list()
        for face in pfaces:
            # get the line segment that source index is not a part of
            f = face.copy().tolist()
            f.remove(source_index)
            # get vertices
            ls.append(vertices[np.array(f)])

        # create a line segment out of the q vector, it should go from the source
        # vertex and extend out 3 units to ensure an intersection
        qls = list()
        source_vertex = vertices[source_index]
        for q_vec in q:
            end_vertex = 5 * q_vec + source_vertex
            qls.append(np.array([end_vertex, source_vertex]))

        # now for each pair of line segments, determine if there is an intersection
        for line_seg, qline_seg, face, q_vec in zip(ls, qls, pfaces, q):
            proportion, pt = self.intersection(line_seg, qline_seg)
            if pt is not False:
                edge = face.copy().tolist()
                edge.remove(source_index)
                # update index to use point closest to proportion
                if proportion >= 0.5:
                    index = edge[0]
                else:
                    index = edge[1]
                return (proportion, edge, pt, index, face, q_vec)

        # if stuff didn't match, it could be because the rotation was wrong
        # we can reflect with q segment along each of the edges from the source vertex
        best_vec = None
        best_angle = 0
        for face in pfaces:
            # grab the edges going out from the source vertex for the face
            points = face.copy().tolist()
            points.remove(source_index)
            vedges = [np.array([source_index, points[0]]), np.array([source_index, points[1]])]
            # now we want to detemine the edge with the closest angle to the nvec
            for vedge in vedges:
                vedge_vertices = vertices[vedge]

                # get the vector to reflect over
                vedge_vec = self.compute_normal_vector(vedge_vertices[1], vedge_vertices[0])

                # compute dot product with nvec
                if np.dot(nvec, vedge_vec) > best_angle:
                    best_angle = np.dot(nvec, vedge_vec)
                    best_vec = vedge_vec

        # now reloop over q_vecs but reflect over best_vec
        if best_vec is not None:
            solutions = list()
            for line_seg, qline_seg, face, q_vec in zip(ls, qls, pfaces, q):
                # reflect the q over the vedge
                new_q = 2 * np.dot(q_vec, best_vec) * best_vec - q_vec

                # create new qline_seg
                end_vertex = 5 * new_q + source_vertex
                qline_seg = np.array([end_vertex, source_vertex])

                # now check intersection
                proportion, pt = self.intersection(line_seg, qline_seg)
                if pt is not False:
                    edge = face.copy().tolist()
                    edge.remove(source_index)
                    # update index to use point closest to proportion
                    if proportion >= 0.5:
                        index = edge[0]
                    else:
                        index = edge[1]
                    # append to solutions list, with how straight the path is
                    # by comparing the perpendicularity of the plane generated
                    # with the surface normal of the current face
                    path_straightness = np.abs(np.dot(np.cross(new_q, nvec), cnormal))
                    solutions.append((path_straightness, proportion, edge, pt, index, face, new_q))

            # return best solution if any
            if len(solutions) > 0:
                solutions.sort(key=lambda x: x[0])
                return solutions[0][1:]

        # if we got here, we couldn't find a intersecting edge
        return (0.0, [-1, -1], np.array([]), 0, np.array([]), np.ndarray([0, 0, 0]))

    def compute_normal(self, face: np.ndarray) -> np.ndarray:
        pts = self.vertices[face]
        vectors = np.diff(pts, axis=0)
        normal = np.cross(vectors[0], vectors[1])
        normal /= np.linalg.norm(normal)
        return normal

    # @staticmethod
    def intersection(
        self, line0: np.ndarray, line1: np.ndarray
    ) -> Tuple[float, Union[np.ndarray, bool], Union[float, None]]:
        # see the following:
        # https://math.stackexchange.com/questions/3114665/how-to-find-if-two-line-segments-intersect-in-3d

        # compute using two points
        # but since singular solutions can occur
        # do each pair of points from the lines
        # than check the solution
        t = list()

        # TODO: DONT REPEAT YOURSELF, DONT REPEAT YOURSELF, DONT REPEAT YOURSELF

        # construct coefficients for linear system (solve with x, y, check with z)
        ab = line0[0, 0:2] - line0[1, 0:2]
        dc = line1[1, 0:2] - line1[0, 0:2]
        coeffs = np.stack((ab, dc), axis=1)

        # construct ordinates
        ordin = line1[1, 0:2] - line0[1, 0:2]

        # solve system
        try:
            t0 = np.linalg.solve(coeffs, ordin)
            t.append(t0)
        except np.linalg.LinAlgError:
            t0 = None

        # construct coefficients for linear system (solve with x, y, check with z)
        ab = line0[0, 1:3] - line0[1, 1:3]
        dc = line1[1, 1:3] - line1[0, 1:3]
        coeffs = np.stack((ab, dc), axis=1)

        # construct ordinates
        ordin = line1[1, 1:3] - line0[1, 1:3]

        # solve system
        try:
            t1 = np.linalg.solve(coeffs, ordin)
            t.append(t1)
        except np.linalg.LinAlgError:
            t1 = None

        # construct coefficients for linear system (solve with x, y, check with z)
        ab = line0[0, np.array([0, 2])] - line0[1, np.array([0, 2])]
        dc = line1[1, np.array([0, 2])] - line1[0, np.array([0, 2])]
        coeffs = np.stack((ab, dc), axis=1)

        # construct ordinates
        ordin = line1[1, np.array([0, 2])] - line0[1, np.array([0, 2])]

        # solve system
        try:
            t2 = np.linalg.solve(coeffs, ordin)
            t.append(t2)
        except np.linalg.LinAlgError:
            t2 = None

        # check that all t's are equal
        t = np.stack(t)
        if not np.allclose(t[0], t):
            return (0.0, False)

        # use the first t that isn't None  # TODO: probably redundant
        for t in [t0, t1, t2]:
            if t is not None:
                break

        # check if each solution is between 0 and 1
        if t[0] < 0 or t[0] > 1 or t[1] < 0 or t[1] > 1:
            return (0.0, False)

        # return intersection point
        return (t[0], t[0] * line0[0] + (1 - t[0]) * line0[1])


# create a search wrapper for executor
def wrap_search(
    cifti_file: str,
    gifti_file: str,
    vertices: np.ndarray,
    faces: np.ndarray,
    face_normals: np.ndarray,
    deleted_faces: np.ndarray,
    index: int,
    radius: float,
    min_angle: float,
    tolerance: float,
    vrtable: np.ndarray,
    offset: int,
):
    # create interdigitation search
    isearch = InterdigitationSearch.create(
        cifti_file, gifti_file, vertices=vertices, faces=faces, face_normals=face_normals, deleted_faces=deleted_faces
    )
    # run search
    result = isearch.search(index, radius, min_angle, tolerance, nospinner=True)

    # convert the interpolated cross-sections into cifti rows
    interpolated_cross_section_cifti_rows = list()
    if len(result[0]) != 0:
        for interpolated_cross_section in result[0]:
            interpolated_cross_section_cifti_rows.append(
                (np.array(interpolated_cross_section).astype("u4") + offset)  # .tolist()  # for JSON output
            )
        if len(interpolated_cross_section_cifti_rows) > 1:  # make an object array if > length 1
            interpolated_cross_section_cifti_rows = np.array(interpolated_cross_section_cifti_rows, dtype=object)
        else:  # hack to make length 1 object array in numpy TODO: Is there an easier way to do this wtf???
            interpolated_cross_section_cifti_rows.append(None)  # make length 2
            # make object array
            interpolated_cross_section_cifti_rows = np.array(interpolated_cross_section_cifti_rows, dtype=object)
            # now drop the last element
            interpolated_cross_section_cifti_rows = interpolated_cross_section_cifti_rows[:1]

    print("Processed vertex: %d" % vrtable[index])
    return (*result, interpolated_cross_section_cifti_rows)


def isearch(
    cifti_file: str,
    gifti_files: List[str],
    radius: float = 20.0,
    min_angle: float = 10.0,
    tolerance: float = 0.5,
    nproc: int = 4,
    vertices_file: str = None,
    vertex_indices: List[str] = None,
    cifti_row_file: List[str] = None,
    cifti_row: List[int] = None,
    debug: bool = False,
    **kwargs,
) -> Dict:
    # create a dictionary
    cross_sections_dict = dict()

    # load the cifti
    cifti = Cifti(cifti_file)

    for gifti_file in gifti_files:
        with yaspin(Spinners.dots12, text="Loading: %s" % basename(gifti_file), color="blue") as sp:
            # load the surface file
            gifti = Gifti(gifti_file)

            # now get a masked copy of the gifti
            masked_gifti = gifti.mask(cifti)

            # get the surface name
            surface_name = masked_gifti.name

            # if vertices/cifti_rows defined, use them
            if vertices_file or vertex_indices:
                if vertices_file:
                    vertices, sn = Movie._load_txt(vertices_file, header=True)
                else:
                    sn = vertex_indices[0]
                    vertices = [int(i) for i in vertex_indices[1:]]
                if surface_name == sn:
                    vertices_to_search = vertices
                else:
                    vertices_to_search = []
            elif cifti_row_file or cifti_row:
                if cifti_row_file:
                    cifti_rows = Movie._load_txt(cifti_row_file)
                else:
                    cifti_rows = cifti_row
                vertex_surfaces = [cifti.row_to_vertex_index(i) for i in cifti_rows]
                vertices_to_search = [v[0] for v in vertex_surfaces if v[1] == surface_name]
            else:  # just load all vertices (no medial wall)
                rv_table = cifti.get_vertex_row_table()
                if surface_name == "CORTEX_LEFT":
                    vertices_to_search = rv_table[0].tolist()
                elif surface_name == "CORTEX_RIGHT":
                    vertices_to_search = rv_table[3].tolist()

            # now convert the vertices_to_search to modified gifti index
            vrtable, _, _ = cifti.get_vrtable(gifti.name)
            vertices_to_search_masked = np.searchsorted(vrtable, vertices_to_search).tolist()

            # get data that is constant for all vertices during search
            vertices = masked_gifti.vertices
            faces = masked_gifti.faces
            face_normals = masked_gifti.face_normals
            deleted_faces = np.array([[-1, -1, -1], [-2, -2, -2]])

            # update spinner status
            sp.text = "Loaded: %s" % basename(gifti_file)
            sp.ok("✅")

        print("Running search on surface %s for vertices: %s" % (surface_name, vertices_to_search))

        # get the vrtable
        try:
            vrtable, offset, _ = cifti.get_vrtable(gifti.name)
        except ValueError:
            print("Skipping invalid surface: %s" % gifti.name)
            continue

        # create a process pool executor
        with ProcessPoolExecutor(max_workers=nproc) as executor:
            if debug:  # single process mapp
                map_func = map
            else:  # multiprocess map
                map_func = executor.map

            # create function to format the data
            def format_data(index, info):
                fdata = {
                    "row_index": np.searchsorted(vrtable, index) + offset,
                    "vertex_index": index,
                    "surface_name": surface_name,
                    # "interpolated_cross_sections": info[0],
                    "cross_sections": info[3],
                    # "exact_cross_sections": info[2],
                    # "exact_cross_sections_positions": info[1],
                }
                if debug:  # for debugging
                    fdata["exact_cross_sections_positions"] = np.array(info[1], dtype=object)
                return fdata

            # store processed vertices in list
            cross_sections_dict[surface_name] = [
                format_data(vertices_to_search[i], cross_section_info)
                for i, cross_section_info in enumerate(
                    map_func(
                        wrap_search,
                        repeat(cifti_file),
                        repeat(gifti_file),
                        repeat(vertices),
                        repeat(faces),
                        repeat(face_normals),
                        repeat(deleted_faces),
                        vertices_to_search_masked,
                        repeat(radius),
                        repeat(min_angle),
                        repeat(tolerance),
                        repeat(vrtable),
                        repeat(offset),
                    )
                )
            ]
            # cross_sections = cross_sections_dict[surface_name][0]["exact_cross_sections_positions"]
            # masked_gifti.render(
            #     [*cross_sections, masked_gifti.vertices[vertices_to_search_masked[0]]],
            #     [*[np.random.rand(3).tolist() for _ in cross_sections], [0, 0, 255]],
            #     [*[0.3 for _ in cross_sections], 0.5]
            # )
            print("Finished processing surface: %s" % surface_name)

    # return dictionary of cross sections
    return cross_sections_dict
