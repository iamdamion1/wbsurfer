from __future__ import annotations
from typing import List, Tuple
from copy import deepcopy

import nibabel as nib
import numpy as np
import trimesh
from trimesh import Trimesh
import pyrender


class Cifti:
    """Object representing Cifti data."""

    def __init__(self, filename: str) -> None:
        self.cifti: nib.Cifti2Image = nib.load(filename)

    def get_vertex_row_table(self) -> Tuple[np.ndarray, int, int, np.ndarray, int, int]:
        """Get vertex row tables."""
        # get axes from header
        dconn_axis = self.cifti.header.get_axis(1)

        # grab left and right cortex
        left_cortex = [s for s in dconn_axis.iter_structures() if "CORTEX_LEFT" in s[0]][0]
        right_cortex = [s for s in dconn_axis.iter_structures() if "CORTEX_RIGHT" in s[0]][0]

        # return vertex tables and offets
        return (
            left_cortex[2].vertex,
            left_cortex[1].start,
            left_cortex[1].stop,
            right_cortex[2].vertex,
            right_cortex[1].start,
            right_cortex[1].stop,
        )

    def get_vrtable(self, surface_name: str) -> Tuple[np.ndarray, int, int]:
        """Get the vrtable for the specified surface."""
        if surface_name == "CORTEX_LEFT":
            return self.get_vertex_row_table()[:3]
        elif surface_name == "CORTEX_RIGHT":
            return self.get_vertex_row_table()[3:]
        else:
            raise ValueError("Invalid Surface Name!")

    def vertex_to_row_index(self, vertex_idx: int, surface: str) -> int:
        """Computes row index from vertex index."""
        # get the vertex row table
        l_vtable, _, _, r_vtable, r_start, _ = self.get_vertex_row_table()

        # figure out which surface we are selecting the vertex from
        # TODO: Test using searchsorted instead of argwhere
        if surface == "CORTEX_LEFT":
            return np.argwhere(l_vtable == vertex_idx)[0, 0]
        elif surface == "CORTEX_RIGHT":
            return np.argwhere(r_vtable == vertex_idx)[0, 0] + r_start

    def row_to_vertex_index(self, row_idx: int) -> Tuple[int, str]:
        """Computes vertex index from row index."""
        # get the vertex row table
        l_vtable, l_start, l_stop, r_vtable, r_start, r_stop = self.get_vertex_row_table()

        # get which surface this row is on
        if row_idx >= l_start and row_idx < l_stop:
            return (l_vtable[row_idx], "CORTEX_LEFT")
        elif row_idx >= r_start and row_idx < r_stop:
            return (r_vtable[row_idx - r_start], "CORTEX_RIGHT")
        else:
            raise ValueError(
                "Uh Oh! (っ- ‸ – ς) Andrew hasn't implemented a vertex look up for me "
                "using the supplied row index! Are you sure the index you have chosen "
                "is on the Cortex Surface? It's the only place I know where to look "
                "at the moment... "
                "If you are sure your row is on the Cortex, message Andrew "
                "with this error message in full.\n"
                f"row_idx: {row_idx}\n"
                f"l_start: {l_start}, l_stop: {l_stop}\n"
                f"r_start: {r_start}, r_stop: {r_stop}\n"
            )

    @property
    def row_data(self) -> np.ndarray:
        return self.cifti.get_fdata()


class Gifti:
    """Object representing Gifti data."""

    def __init__(self, filename: str) -> None:
        self.surface: nib.GiftiImage = nib.load(filename)

        # get agg_data
        self.agg_data = self.surface.agg_data()

        # convert to float64
        self.agg_data = (self.agg_data[0].astype("f8"), self.agg_data[1])

        # save mesh (set process to False to preserve ordering)
        self.mesh = Trimesh(vertices=self.vertices, faces=self.triangles, process=False)

    def vertex_indices_to_positions(self, vertex_indices: List[int]) -> np.ndarray:
        return self.agg_data[0][vertex_indices]

    @staticmethod
    def diagonal_dot(a: np.ndarray, b: np.ndarray) -> float:
        """
        Dot product by row of a and b.
        There are a lot of ways to do this though
        performance varies very widely. This method
        uses a dot product to sum the row and avoids
        function calls if at all possible.
        """
        a = np.asanyarray(a)
        return np.dot(a * b, [1.0] * a.shape[1])

    @property
    def name(self) -> str:
        """Returns the surface name."""
        # get the surface name that is loaded in
        primary_anat = self.surface.darrays[0].metadata["AnatomicalStructurePrimary"]
        return {"CortexLeft": "CORTEX_LEFT", "CortexRight": "CORTEX_RIGHT"}[primary_anat]

    @property
    def num_vertices(self) -> int:
        return self.agg_data[0].shape[0]

    @property
    def vertices(self) -> np.ndarray:
        return self.agg_data[0]

    @property
    def num_triangles(self) -> int:
        return self.agg_data[1].shape[0]

    @property
    def triangles(self) -> np.ndarray:
        return self.faces

    @property
    def faces(self) -> np.ndarray:
        return self.agg_data[1]

    @property
    def face_normals(self) -> np.ndarray:
        return self.mesh.face_normals

    @property
    def vertex_normals(self) -> np.ndarray:
        return self.mesh.vertex_normals

    def mask(self, cifti: Cifti) -> Gifti:
        """Returns a masked version of the gifti surface as defined by the cifti file."""
        # get the vrtable for the surface
        vrtable, _, _ = cifti.get_vrtable(self.name)

        # copy this Gifti object
        gifti = deepcopy(self)

        # mask vertices
        vertices = gifti.vertices[vrtable]

        # mask faces (and use updated index)
        face_mask = np.all(np.isin(gifti.faces, vrtable), axis=1)
        masked_faces = gifti.faces[face_mask]
        faces = np.searchsorted(vrtable, masked_faces)

        # substitute in agg_data
        gifti.agg_data = (vertices, faces)

        # recreate the mesh
        gifti.mesh = Trimesh(vertices=vertices, faces=faces, process=False)

        # return the modified gifti
        return gifti

    def render(
        self, points: List[np.ndarray] = None, colors: List[List[float]] = None, sizes: List[float] = None
    ) -> None:
        """Renders the mesh. Must have pyrender installed!"""
        scene = pyrender.Scene(bg_color=[0, 0, 0, 0])
        mesh = pyrender.Mesh.from_trimesh(self.mesh)
        scene.add(mesh)
        if points is not None:
            if colors is None:
                colors = [np.random.rand(3).tolist() for _ in points]
            if sizes is None:
                sizes = [0.05 for _ in points]
            for p, c, s in zip(points, colors, sizes):
                sm = trimesh.creation.uv_sphere(radius=s)
                sm.visual.vertex_colors = c
                tfs = np.tile(np.eye(4), (len(p), 1, 1))
                tfs[:, :3, 3] = p
                point_mesh = pyrender.Mesh.from_trimesh(sm, poses=tfs)
                scene.add(point_mesh)
        pyrender.Viewer(scene, use_raymond_lighting=True)
