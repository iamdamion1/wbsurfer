from subprocess import CalledProcessError
from .border import Border
from .movie import Movie
from .scene import Scene
from .utils import run_ffmpeg

# check that ffmpeg installed
try:
    run_ffmpeg("-version", capture_output=True)
except CalledProcessError:
    raise CalledProcessError("The ffmpeg package not installed. It is required for this program to function.")
